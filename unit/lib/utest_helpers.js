const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Unit = imports.third_party.gjsunit.gjsunit;

function assertThrows(testFunction, expectedError) {
    try {
        testFunction();
    }
    catch (err) {
        if (err.name === expectedError) {
            return err.message;
        }
        Unit.fail('Wrong error thrown, expected ' + expectedError + ', received ' + err.name + ': ' + err.message);
        return err.message;
    }
    Unit.fail('No error thrown, expected ' + expectedError);
    return '';
}

function deleteFile(file) {
    return file['delete'](null);
}

function makeTemporaryFile(contents) {
    let [file, stream] = Gio.File.new_tmp('XXXXXX.todo.txt_utest.txt');
    stream.output_stream.write(contents, null);
    stream.close(null);
    return file;
}

function makeTemporaryDirContainingDir(name) {
    let dir = GLib.Dir.make_tmp('XXXXXX-todo.txt_utest');
    let subdir = Gio.File.new_for_path(GLib.build_filenamev([dir, name]));
    subdir.make_directory_with_parents(null);
    return dir;
}

function makeFileInTemporaryDir(filename, dirname, contents) {
    let dir = makeTemporaryDirContainingDir(dirname);
    let path = GLib.build_filenamev([dir, dirname, filename]);
    let file = Gio.File.new_for_path(path);
    let stream = file.replace(null, false, Gio.FileCreateFlags.NONE, null);
    stream.write(contents, null);
    stream.close(null);
    return [dir, file.get_path()];
}

/* vi: set expandtab tabstop=4 shiftwidth=4: */
