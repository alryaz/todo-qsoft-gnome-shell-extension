// Stripped down version of gnome fileUtils.js

const Gio = imports.gi.Gio;

function deleteGFile(file) {
    // Work around 'delete' being a keyword in JS.
    return file['delete'](null);
}

function recursivelyDeleteDir(dir, deleteParent) {
    let children = dir.enumerate_children('standard::name,standard::type',
        Gio.FileQueryInfoFlags.NONE, null);

    let info, child;
    while ((info = children.next_file(null)) !== null) {
        let type = info.get_file_type();
        let child = dir.get_child(info.get_name());
        if (type == Gio.FileType.REGULAR) {
            deleteGFile(child);
        }
        else if (type == Gio.FileType.DIRECTORY) {
            recursivelyDeleteDir(child, true);
        }
    }

    if (deleteParent) {
        deleteGFile(dir);
    }
}

/* vi: set expandtab tabstop=4 shiftwidth=4: */
