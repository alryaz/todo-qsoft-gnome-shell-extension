const Helper = imports.unit.lib.utest_helpers;
const JsTextFile = imports.jsTextFile;
const Shell = imports.gi.Shell;
const Unit = imports.third_party.gjsunit.gjsunit;
const Utils = imports.utils;

var init = new Unit.Suite('jsTextFile init');

init.addTest('create without path', function() {
    let message = Helper.assertThrows(function() {
        new JsTextFile.JsTextFile();
    }, 'IoError');
    Unit.assertEquals('JsTextFile: no path specified', message);
});

init.addTest('create with non-existing path', function() {
    let message = Helper.assertThrows(function() {
        new JsTextFile.JsTextFile('/foo/bar/this/does/not/exist/I/hope');
    }, 'IoError');
    Unit.assertEquals('JsTextFile: trying to load non-existing file /foo/bar/this/does/not/exist/I/hope',
        message);
});

var normalFile = new Unit.Suite('jsTextFile normal file');

normalFile.setup = function() {
    normalFile._tempFile = Helper.makeTemporaryFile('test1\ntest2\ntest3\n');
    normalFile.textFile = new JsTextFile.JsTextFile(normalFile._tempFile.get_path(), Utils.getDefaultLogger());
};

normalFile.teardown = function() {
    Helper.deleteFile(normalFile._tempFile);
};

normalFile.addTest('get line number', function() {
    Unit.assertEquals(1, normalFile.textFile._getLineNum('test2'));
    Unit.assertEquals(0, normalFile.textFile._getLineNum('test1'));
    Unit.assertEquals(2, normalFile.textFile._getLineNum('test3'));
});

var emptyLinesFile = new Unit.Suite('jsTextFile file with empty lines');

emptyLinesFile.setup = function() {
    emptyLinesFile._tempFile = Helper.makeTemporaryFile('test1\n\n\ntest2\n\ntest3\n\n\n');
    emptyLinesFile.textFile = new JsTextFile.JsTextFile(emptyLinesFile._tempFile.get_path());
};

emptyLinesFile.teardown = function() {
    Helper.deleteFile(emptyLinesFile._tempFile);
};

emptyLinesFile.addTest('remove empty lines', function() {
    emptyLinesFile.textFile._removeEmptyLines();
    Unit.assertEquals(emptyLinesFile.textFile.getLines().join(), 'test1,test2,test3');
});

emptyLinesFile.addTest('save file without removing empty lines', function() {
    emptyLinesFile.textFile.saveFile(false);
    let content = Shell.get_file_contents_utf8_sync(emptyLinesFile._tempFile.get_path());
    Unit.assertEquals(content, 'test1\n\n\ntest2\n\ntest3\n\n\n');
});

emptyLinesFile.addTest('save file with empty lines removed', function() {
    emptyLinesFile.textFile.saveFile(true);
    let content = Shell.get_file_contents_utf8_sync(emptyLinesFile._tempFile.get_path());
    Unit.assertEquals(content, 'test1\ntest2\ntest3\n');
});

emptyLinesFile.addTest('get lines from file', function() {
    Unit.assertArrayEquals(emptyLinesFile.textFile.getLines(), ['test1', '', '', 'test2', '', 'test3', '', '']);
});

normalFile.addTest('delete lines', function() {
    Unit.assertTrue(normalFile.textFile.removeLine('test2'));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test1', 'test3']);

    Unit.assertTrue(normalFile.textFile.removeLine('test1'));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test3']);
});

normalFile.addTest('delete non-existing lines', function() {
    Unit.assertFalse(normalFile.textFile.removeLine('test5'));
    Unit.assertFalse(normalFile.textFile.removeLine(''));
});

normalFile.addTest('add lines', function() {
    Unit.assertTrue(normalFile.textFile.addLine('test4'));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test1', 'test2', 'test3', 'test4']);

    Unit.assertTrue(normalFile.textFile.addLine('test5'));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test1', 'test2', 'test3', 'test4', 'test5']);
});

normalFile.addTest('add lines at front', function() {
    Unit.assertTrue(normalFile.textFile.addLine('test4', true));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test4', 'test1', 'test2', 'test3']);

    Unit.assertTrue(normalFile.textFile.addLine('test5', true));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test5', 'test4', 'test1', 'test2', 'test3']);
});

normalFile.addTest('modify lines', function() {
    Unit.assertTrue(normalFile.textFile.modifyLine('test2', 'test4'));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test1', 'test4', 'test3']);

    Unit.assertTrue(normalFile.textFile.modifyLine('test3', 'test2'));
    Unit.assertArrayEquals(normalFile.textFile.getLines(), ['test1', 'test4', 'test2']);
});

normalFile.addTest('modify non-existing line', function() {
    Unit.assertFalse(normalFile.textFile.modifyLine('test5', 'test6'));
    Unit.assertFalse(normalFile.textFile.modifyLine('', 'test6'));
});

normalFile.addTest('set all lines', function() {
    let expect = ['test4', 'test5', 'test6'];
    normalFile.textFile.setLines(expect);
    Unit.assertArrayEquals(normalFile.textFile.getLines(), expect);
});

/* vi: set expandtab tabstop=4 shiftwidth=4: */
